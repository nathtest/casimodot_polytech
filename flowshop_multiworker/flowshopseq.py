from __future__ import absolute_import, division, print_function, unicode_literals
import csv
import numpy as np
import tensorflow as tf
import os
import json

from tensorflow.keras import layers
from tensorflow.keras.layers import Bidirectional
from tensorflow.keras.callbacks import CSVLogger

import tensorflow_on_slurm

csv_logger = CSVLogger('log.csv', append=True, separator=';')

tf.compat.v1.disable_eager_execution()


os.environ['TF_CONFIG'] = json.dumps(tensorflow_on_slurm.keras_config_from_slurm(ps_number=0, port_number=2223))  # 0 index to only take cluster dict

print(os.environ['TF_CONFIG'])

strategy = tf.distribute.experimental.MultiWorkerMirroredStrategy()


def saveLinewithC(line):
    """
    Supplement function for bulid the diffrent sets of the seq2seq model.

    :param line: one line of the csv file
    :return: ptimes_list, solved_list.

    ptimes_list: the sequence with processing time and completion time.
    solved_list: the binary sequence that can indicate the window.
    """
    # print(line)
    ptimes = line[0].split(' ')
    ptimes_list = []
    # print(ptimes)
    for k in range(1, len(ptimes), 4):
        ptimes_list.append(
            [int(float(ptimes[k])), int(float(ptimes[k + 1])), int(float(ptimes[k + 2])),
             int(float(ptimes[k + 3]))])
    solved_list = list(map(int, line[1]))

    return ptimes_list, solved_list


def divideDatawithC(size, num_instance):
    """
    Function can divide the data into 3 part: Training set, Test set and Validation set.

    :param txtfile: the path of the database txt file.
    :param size: size of the sequence
    :return: X_train, y_train, X_test, y_test, X_validation, y_validation
    X_train: Training set which have the initial sequence with processing time and completion time.
    y_train: Training set which have the binary sequnece that can indicate the window.
    X_test: Test set which have the initial sequence with processing time and completion time.
    y_test: Test set which have the binary sequnece that can indicate the window.
    X_validation: Validation set which have the initial sequence with processing time and completion time.
    y_validation: Validation set which have the binary sequnece that can indicate the window.

    """

    print("num_instance: " + str(num_instance))
    num_ins_test = int(num_instance * 0.2)
    num_ins_validation = num_ins_test
    num_ins_train = num_instance - num_ins_test * 2
    X_train = []
    y_train = []
    X_test = []
    y_test = []
    X_validation = []
    y_validation = []
    with open('databaseC.csv') as data:
        reader = csv.reader(data)
        dataSet = list(reader)
        length = len(dataSet)
        count = 0
        for line in dataSet:
            if len(line) == 1:
                count = count + 1
                continue
            if count <= num_ins_train:
                ptimes_list, solved_list = saveLinewithC(line)
                X_train.append(ptimes_list)
                y_train.append(solved_list)
            if num_ins_train < count <= num_ins_train + num_ins_test:
                ptimes_list, solved_list = saveLinewithC(line)
                X_test.append(ptimes_list)
                y_test.append(solved_list)
            if num_ins_train + num_ins_test < count <= num_instance:
                ptimes_list, solved_list = saveLinewithC(line)
                X_validation.append(ptimes_list)
                y_validation.append(solved_list)
    X_train = np.asarray(X_train)
    y_train = np.asarray(y_train)
    y_train = np.reshape(y_train, (len(y_train), size, 1))
    X_test = np.asarray(X_test)
    y_test = np.asarray(y_test)
    y_test = np.reshape(y_test, (len(y_test), size, 1))
    X_validation = np.asarray(X_validation)
    y_validation = np.asarray(y_validation)
    y_validation = np.reshape(y_validation, (len(y_validation), size, 1))
    return X_train, y_train, X_test, y_test, X_validation, y_validation


with strategy.scope():
    input_dim = 4  # mode with completion time by default
    num_instance = 4448  # num of instances, maximum 4448 by default
    jobs_size = 100  # size of jobs = size of input

    batch_size = 256
    epochs = 10  # 5 epochs by default
    hidden_dim = 1000  # nb neuron by Layer

    timesteps = jobs_size
    data_dim = input_dim

    X_train, y_train, X_test, y_test, X_validation, y_validation = divideDatawithC(jobs_size, num_instance)
    print("X_train=", X_train.shape)
    print("y_train=", y_train.shape)

    train_dataset = tf.data.Dataset.from_tensor_slices((X_train, y_train))

    BUFFER_SIZE = 10000

    train_dataset = train_dataset.shuffle(num_instance).repeat().batch(batch_size)

    if num_instance % batch_size != 0:
        parallel_steps = num_instance // batch_size + 1
    else:
        parallel_steps = num_instance // batch_size

    # A Three-layers model
    model = tf.compat.v1.keras.Sequential()

    model.add(Bidirectional(
        layers.LSTM(hidden_dim, return_sequences=True, stateful=False,
                    batch_input_shape=(batch_size, timesteps, data_dim)),
        input_shape=(timesteps, data_dim)))  # returns a sequence of vectors of dimension hidden_dim

    model.add(Bidirectional(
        layers.LSTM(hidden_dim, return_sequences=True)))  # returns a sequence of vectors of dimension hidden_dim

    model.add(layers.LSTM(1, return_sequences=True))  # returns a sequence of vectors of dimension 1

    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

    model.summary()

    # history = model.fit(X_train, y_train, validation_data=(X_validation, y_validation), epochs=epochs,
    #                     batch_size=batch_size, callbacks=[csv_logger])

    history = model.fit(train_dataset, epochs=epochs, steps_per_epoch=parallel_steps, callbacks=[csv_logger])

    print(history.history.keys())
# summarize history for loss
# plt.plot(history.history['loss'])
# plt.plot(history.history['val_loss'])
# plt.title('model loss')
# plt.ylabel('loss')
# plt.xlabel('epoch')
# plt.legend(['train', 'test'], loc='upper left')
# plt.show()
